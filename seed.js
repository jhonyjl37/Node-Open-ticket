var seeder = require('./seeds/seeder');
console.log("Iniciando seed...");

// Adicionando usuários.
seeder.InsertGroup(
  'users',
  [
  	{
	  	name: "Admin",
	  	rg: "000.000.000",
	  	cpf: "000.000.000",
	  	date_of_birth: "2015-05-03",
	  	user_name: "Admin",
	  	email: "admin@email.com",
	  	password: "123mudar",
	  	gender: "m",
	  	privileges: "admin",
	  	campus: "none",
	  	role: "admin"
	  }
  ]
);


// Adicionando Setores.
seeder.InsertGroup(
  'sectors',
  [
	  {	name: "Desenvolvimento" },
	  { name: "T.I"}
  ]
);