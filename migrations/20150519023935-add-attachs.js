var dbm = global.dbm || require('db-migrate');
var type = dbm.dataType;

exports.up = function(db, callback) {
  db.createTable('attachs', {
    id:            { type: 'int', primaryKey: true, autoIncrement: true },
    path:          { type: 'string' },
    create_on:     { type: 'datetime', notNull: true },
    message_id:  { 
      type: 'int',
      foreignKey: { table: 'ticketshistory' },
      mapping:    { message_id: 'id' }
    },
    ticket_id:  { 
      type: 'int',
      foreignKey: { table: 'tickets' },
      mapping:    { ticket_id: 'id' }
    }
  }, callback);
};

exports.down = function(db, callback) {
  db.dropTable('attachs', callback);
};
