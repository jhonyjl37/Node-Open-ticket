var dbm = global.dbm || require('db-migrate');
var type = dbm.dataType;

exports.up = function(db, callback) {
  db.createTable('ticketshistory', {
    id:            { type: 'int', primaryKey: true, autoIncrement: true },
    message:       { type: 'text', notNull: true },
    create_on:     { type: 'datetime', notNull: true },
    user_id:  { 
      type: 'int',
      notNull: true ,
      foreignKey: { table: 'users' },
      mapping:    { user_id: 'id' }
    },
    ticket_id:  { 
      type: 'int',
      notNull: true ,
      foreignKey: { table: 'tickets' },
      mapping:    { ticket_id: 'id' }
    }
  }, callback);
};

exports.down = function(db, callback) {
  db.dropTable('ticketshistory', callback);
};
