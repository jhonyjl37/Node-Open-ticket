var dbm = global.dbm || require('db-migrate');
var type = dbm.dataType;

exports.up = function(db, callback) {
  db.createTable('sector', {
    id:    { type: 'int', primaryKey: true, autoIncrement: true },
    sector_id:  { 
      type: 'int',
      notNull: true ,
      foreignKey: { table: 'sectors' },
      mapping:    { sector_id: 'id' }
    },
    user_id:  { 
      type: 'int',
      notNull: true ,
      foreignKey: { table: 'users' },
      mapping:    { user_id: 'id' }
    }
  }, callback);
};

exports.down = function(db, callback) {
  db.dropTable('sector', callback);
};
