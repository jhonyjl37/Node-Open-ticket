var dbm = global.dbm || require('db-migrate');
var type = dbm.dataType;

exports.up = function(db, callback) {
  db.createTable('users', {
    id:            { type: 'int', primaryKey: true, autoIncrement: true },
    name:          { type: 'string', notNull: true },
    rg:            { type: 'string', notNull: true },
    cpf:           { type: 'string', notNull: true },
    date_of_birth: { type: 'date',   notNull: true },
    user_name:     { type: 'string', notNull: true, unique: true },
    email:         { type: 'string', notNull: true, unique: true },
    password:      { type: 'string', notNull: true },
    gender:        { type: 'char',   notNull: true },
    privileges:    { type: 'string', notNull: true },
    campus:        { type: 'string', notNull: true },
    role:          { type: 'string', notNull: true }

  }, callback);
};

exports.down = function(db, callback) {
  db.dropTable('users', callback);
};
