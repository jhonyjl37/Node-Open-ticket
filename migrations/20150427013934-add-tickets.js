var dbm = global.dbm || require('db-migrate');
var type = dbm.dataType;

exports.up = function(db, callback) {
  db.createTable('tickets', {
    id:            { type: 'int', primaryKey: true, autoIncrement: true },
    protocol:      { type: 'int', notNull: true },
    status:        { type: 'string', notNull: true },
    subject:       { type: 'string', notNull: true },
    description:   { type: 'text', notNull: true },
    prompt:			   { type: 'date', notNull: true },
    user_id:  { 
      type: 'int',
      notNull: true ,
      foreignKey: { table: 'users' },
      mapping:    { user_id: 'id' }
    },
    sector_id:  { 
      type: 'int',
      notNull: true ,
      foreignKey: { table: 'sectors' },
      mapping:    { sector_id: 'id' }
    },
    support_id:  { 
      type: 'int',
      foreignKey: { table: 'users' },
      mapping:    { support_id: 'id' }
    }
  }, callback);
};

exports.down = function(db, callback) {
  db.dropTable('tickets', callback);
};
