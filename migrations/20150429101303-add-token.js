var dbm = global.dbm || require('db-migrate');
var type = dbm.dataType;

exports.up = function(db, callback) {
  db.createTable('tokens', {
    id:            { type: 'int', primaryKey: true, autoIncrement: true },
    token:         { type: 'string', notNull: true },
    create_on:     { type: 'datetime', notNull: true },
    user_id:  { 
      type: 'int',
      notNull: true ,
      foreignKey: { table: 'users' },
      mapping:    { user_id: 'id' }
    }
  }, callback);
};

exports.down = function(db, callback) {
  db.dropTable('tokens', callback);
};
