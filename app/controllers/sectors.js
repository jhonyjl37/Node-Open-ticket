var pg = require('pg'); 

module.exports = function(app) {
	var controller = {};

	// Listar todos os setores
	controller.list = function(req,res){
    	pg.connect(app.connectionString, function(err, client, done) {

            var query = client.query("SELECT name, COUNT(sector_id) AS count, sectors.id AS id  FROM sectors LEFT JOIN sector ON sectors.id = sector.sector_id GROUP BY name, sectors.id ORDER BY id ASC;",function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }
            });
            var results = [];

            query.on('row', function(row) {
                results.push(row);
            });

            query.on('end', function() {
                client.end();
                return res.json(results);
            });

            if(err) {
              console.log(err);
            }

        });
	};

	// Adicionar novo setor
	controller.new = function(req,res){

		var seed = {
			name: req.body.name
		};

    	pg.connect(app.connectionString, function(err, client, done) {

            var query = client.query(app.seeder.getInsertSQL(seed,'sectors'),function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }
            });

            query.on('end', function() {
                client.end();
                return res.send("Setor adicionado com sucesso.");
            });

            if(err) {
              console.log(err);
            }

        });

	};

	// Atualizar setor
	controller.update = function(req,res){

		var seed = {
			id: req.body.id,
			name: req.body.name
		};

		pg.connect(app.connectionString, function(err, client, done) {

        var query = client.query(app.seeder.getUpdateSQL(seed,'sectors'),function(err, result) {
            if(err) {
              console.error('error running query', err);
              return res.status(500).send('Something broke!');
            }
        });

        query.on('end', function() {
            client.end();
            return res.send("Setor atualizado com sucesso.");
        });

        if(err) {
          console.log(err);
        }

    });

	};

	controller.delete = function(req,res){

		var seed = {
			id: req.query.id
		};

    	pg.connect(app.connectionString, function(err, client, done) {

    		var sql = "DELETE FROM sectors WHERE id NOT IN (SELECT sector_id FROM sector) AND id = "+seed.id+";";
            var query = client.query(sql,function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }
            });

            query.on('end', function() {
                client.end();
                return res.send("Setor removido com sucesso.");
            });

            if(err) {
              console.log(err);
            }

        });

	};

    controller.associations = function(req,res){
        var id = req.query.id;

        pg.connect(app.connectionString, function(err, client, done) {

            var query = client.query("SELECT sector.id AS id, sector_id,user_id, name FROM sector INNER JOIN sectors ON sector.sector_id = sectors.id WHERE user_id = "+id+";",function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }
            });
            var results = [];

            query.on('row', function(row) {
                results.push(row);
            });

            query.on('end', function() {
                client.end();
                return res.json(results);
            });

            if(err) {
              console.log(err);
            }

        });
    };

        // Adicionar novo setor
    controller.newAssociation = function(req,res){

        var seed = {
            user_id: req.body.user_id,
            sector_id: req.body.sector_id
        };

        pg.connect(app.connectionString, function(err, client, done) {

            var query = client.query(app.seeder.getInsertSQL(seed,'sector'),function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }
            });

            query.on('end', function() {
                client.end();
                return res.send("Setor adicionado com sucesso.");
            });

            if(err) {
              console.log(err);
            }

        });

    };

    controller.deleteAssociation = function(req,res){

        var seed = {
            id: req.query.id
        };

        pg.connect(app.connectionString, function(err, client, done) {

            var sql = app.seeder.getDeleteSQL(seed,'sector');
            var query = client.query(sql,function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }
            });

            query.on('end', function() {
                client.end();
                return res.send("Setor removido com sucesso.");
            });

            if(err) {
              console.log(err);
            }

        });

    };

    controller.getUsersBySector = function(req,res){
        var id = req.query.id;

        pg.connect(app.connectionString, function(err, client, done) {

            var query = client.query("SELECT users.id AS id, users.name AS name FROM sector INNER JOIN users ON sector.user_id = users.id WHERE sector.sector_id = "+id+";",function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }
            });
            var results = [];

            query.on('row', function(row) {
                results.push(row);
            });

            query.on('end', function() {
                client.end();
                return res.json(results);
            });

            if(err) {
              console.log(err);
            }

        });
    };

	return controller;
};