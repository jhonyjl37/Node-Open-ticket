var pg = require('pg'); 

var error = function(err, result) {
	if(err) {
      return console.error('error running query', err);
    }
};

module.exports = function(app) {
	var controller = {};

	
	controller.show = function(req,res){
		pg.connect(app.connectionString, function(err, client, done) {

			var id = req.query.id;
	        var query = client.query("SELECT * FROM users WHERE id = " + id + ";",function(err, result) {
				if(err) {
				  console.error('error running query', err);
			      return res.status(500).send('Something broke!');
			    }
			});
			
	        var results = [];

	        query.on('row', function(row) {
	            results.push(row);
	        });

	        query.on('end', function() {
	            client.end();
	            return res.json(results[0]);
	        });

	        if(err) {
	          console.log(err);
	        }

    	});
	};

	// Listar todos os usuários
	controller.list = function(req,res){
		pg.connect(app.connectionString, function(err, client, done) {

	        var query = client.query("SELECT * FROM users ORDER BY id DESC",function(err, result) {
				if(err) {
				  console.error('error running query', err);
			      return res.status(500).send('Something broke!');
			    }
			});
			
	        var results = [];

	        query.on('row', function(row) {
	            results.push(row);
	        });

	        query.on('end', function() {
	            client.end();
	            return res.json(results);
	        });

	        if(err) {
	          console.log(err);
	        }

    	});
	};

	// Adicionar novo usuário
	controller.new = function(req,res){

		var seed = {
			name: req.body.name,
			rg: req.body.rg,
			cpf: req.body.cpf,
			date_of_birth: req.body.date_of_birth,
			user_name: req.body.user_name,
			email: req.body.email,
			password: req.body.password,
			gender: req.body.gender,
			privileges: req.body.privileges,
			campus: req.body.campus,
			role: req.body.role
		};

		pg.connect(app.connectionString, function(err, client, done) {

        var query = client.query(app.seeder.getInsertSQL(seed,'users'),function(err, result) {
			if(err) {
			  console.error('error running query', err);
		      return res.status(500).send('Something broke!');
		    }
		});

        query.on('end', function() {
            client.end();
            return res.send("Usuário adicionado com sucesso.");
        });

        if(err) {
          console.log(err);
        }

    });

	};

	// Atualizar usuário
	controller.update = function(req,res){

		var seed = req.body.user;

		pg.connect(app.connectionString, function(err, client, done) {

            var query = client.query(app.seeder.getUpdateSQL(seed,'users'),function(err, result) {
				if(err) {
				  console.error('error running query', err);
			      return res.status(500).send('Something broke!');
			    }
			});

	        query.on('end', function() {
	            client.end();
	            return res.send("Usuário atualizado com sucesso.");
	        });

	        if(err) {
	          console.log(err);
	        }

	    });

	};


	// Remover usuário
	controller.delete = function(req,res){

		var seed = {
			id: req.query.id
		};

		pg.connect(app.connectionString, function(err, client, done) {

	    var sql = app.seeder.getDeleteSQL(seed,'users');
        var query = client.query(sql,function(err, result) {
			if(err) {
			  console.error('error running query', err);
		      return res.status(500).send('Something broke!');
		    }
		});

        query.on('end', function() {
            client.end();
            return res.send("Usuário removido com sucesso.");
        });

        if(err) {
          console.log(err);
        }

    });

	};

	controller.unique = function(req, res){

		var key = req.body.key;
		var value = req.body.value;
		pg.connect(app.connectionString, function(err, client, done) {

	        var query = client.query("SELECT * FROM users WHERE "+ key +" = '"+value+"' ;",function(err, result) {
				if(err) {
				  console.error('error running query', err);
			      return res.status(500).send('Something broke!');
			    }
			});
			
	        var results = [];

	        query.on('row', function(row) {
	            results.push(row);
	        });

	        query.on('end', function() {
	            client.end();

	            if(results.length == 0){
	              return res.send("unique");
	            }else{
	              return res.send("duplicate");
	            }
	        });

	        if(err) {
	          console.log(err);
	        }

    	});
	}


	// Listar usuários com filtro
	controller.filter = function(req,res){

		var key = req.body.key;
		var value  = req.body.value; 

		pg.connect(app.connectionString, function(err, client, done) {

	        var query = client.query("SELECT * FROM users WHERE "+key+" = '"+ value+"' ORDER BY id ASC;",function(err, result) {
				if(err) {
				  console.error('error running query', err);
			      return res.status(500).send('Something broke!');
			    }
			});
			
	        var results = [];

	        query.on('row', function(row) {
	            results.push(row);
	        });

	        query.on('end', function() {
	            client.end();
	            return res.json(results);
	        });

	        if(err) {
	          console.log(err);
	        }

    	});
	};

	// Atualizar senha do usuário
	controller.updatePassword = function(req,res){

		var seed = {
			user_id: req.body.user_id,
			password: req.body.password,
			new_password: req.body.new_password
		};

		pg.connect(app.connectionString, function(err, client, done) {

            var query = client.query("UPDATE users SET password = '"+seed.new_password+"' WHERE id = "+seed.user_id+" AND password = '"+seed.password+"';",function(err, result) {
				if(err) {
				  console.error('error running query', err);
			      return res.status(500).send('Something broke!');
			    }
			});

	        query.on('end', function(status) {
	            client.end();
	            if(status.rowCount == 1){
	            	return res.send("Usuário atualizado com sucesso.");
	            }else{
	            	return res.status(403).send('Não autorizado!');
	            }
	        });

	        if(err) {
	          console.log(err);
	        }

	    });

	};

	return controller;
};