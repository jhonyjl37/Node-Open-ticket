var pg = require('pg'); 

module.exports = function(app) {
	var controller = {};

	// Listar todos os telefones
	controller.list = function(req,res){
		pg.connect(app.connectionString, function(err, client, done) {
        var id = req.query.id;

        var query = client.query("SELECT * FROM phones WHERE user_id = "+ id +" ORDER BY id ASC",function(err, result) {
            if(err) {
              console.error('error running query', err);
              return res.status(500).send('Something broke!');
            }
        });
        var results = [];

        query.on('row', function(row) {
            results.push(row);
        });

        query.on('end', function() {
            client.end();
            return res.json(results);
        });

        if(err) {
          console.log(err);
        }

    });
	};

	// Adicionar novo telefone
	controller.new = function(req,res){

		var seed = {
			user_id: req.body.user_id,
            number:  req.body.number
		};

		pg.connect(app.connectionString, function(err, client, done) {

        var query = client.query(app.seeder.getInsertSQL(seed,'phones'),function(err, result) {
            if(err) {
              console.error('error running query', err);
              return res.status(500).send('Something broke!');
            }
        });

        query.on('end', function() {
            client.end();
            return res.send("Telefone adicionado com sucesso.");
        });

        if(err) {
          console.log(err);
        }

    });

	};

	// Atualizar telefone
	controller.update = function(req,res){

		var seed = {
			id: req.body.id,
			number: req.body.number
		};

		pg.connect(app.connectionString, function(err, client, done) {

        var query = client.query(app.seeder.getUpdateSQL(seed,'phones'),function(err, result) {
            if(err) {
              console.error('error running query', err);
              return res.status(500).send('Something broke!');
            }
        });

        query.on('end', function() {
            client.end();
            return res.send("Telefone atualizado com sucesso.");
        });

        if(err) {
          console.log(err);
        }

    });

	};


	// Remover telefone
	controller.delete = function(req,res){

		var seed = {
			id: req.query.id
		};

		pg.connect(app.connectionString, function(err, client, done) {

		var sql = app.seeder.getDeleteSQL(seed,'phones');
        var query = client.query(sql,function(err, result) {
            if(err) {
              console.error('error running query', err);
              return res.status(500).send('Something broke!');
            }
        });

        query.on('end', function() {
            client.end();
            return res.send("Telefone removido com sucesso.");
        });

        if(err) {
          console.log(err);
        }

    });

	};

	return controller;
};