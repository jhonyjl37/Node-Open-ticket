var pg = require('pg');

module.exports = function(app) {
  var controller = {};

  controller.upload = function(req, res){

    var seed = {
      path: req.files.file.name
    };

    pg.connect(app.connectionString, function(err, client, done) {

      var query = client.query(app.seeder.getInsertSQL(seed,'attachs'),function(err, result) {
        if(err) {
          console.error('error running query', err);
            return res.status(500).send('Something broke!');
          }
      });

      query.on('end', function(data) {
          client.end();
          return res.json({ path: req.files.file.name });
      });

      if(err) {
        console.log(err);
      }

    });

  };

  return controller;
};