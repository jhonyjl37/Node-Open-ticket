var pg = require('pg');
var nodemailer = require('nodemailer');

module.exports = function(app) {
	var controller = {};

	controller.sendEmail = function(req, res){

		  var email = req.body.email;

		  if(email == null || email == undefined) return res.status(500).send('No match param!');

    	pg.connect(app.connectionString, function(err, client, done) {

        var query = client.query("SELECT id, email, user_name, password FROM users WHERE email = '"+email+"' ;",function(err, result) {
          if(err) {
            console.error('error running query', err);
            return res.status(500).send('Something broke!');
          }
        });

        var result = null;

        query.on('row', function(row) {
          result = row;
        });

        query.on('end', function() {
          client.end();

          if(result == null || result == undefined) return res.status(500).send('No match user!');

          console.log(JSON.stringify(result));

          var transporter = nodemailer.createTransport({
					    service: 'Gmail',
					    auth: {
					        user: 'openticket.contato@gmail.com',
					        pass: 'ticket54321'
					    }
					});

					var mailOptions = {
					    from: 'Open Ticket <foo@blurdybloop.com>', // sender address
					    to: result.email, // list of receivers   
					    subject: 'Open Ticket: senha do usuário '+ result.user_name, // Subject line
					    text: 'Você recebeu esse email devido uma solicitação de perda de senha. Para entrar no sistema como '+result.user_name+' deverá utilizar a senha: '+result.password, // plaintext body
					    html: '' 
					};

					// send mail with defined transport object
					transporter.sendMail(mailOptions, function(error, info){
					    if(error){
					        console.log(error);
					        return res.status(500).send('Something broke!');
					    }else{
					        console.log('Message sent: ' + info.response);
					        return res.send("Senha enviada para email do usuário.");
					    }
					});
          
        });

        if(err) {
          console.log(err);
        }

    });
	};

	return controller;
};