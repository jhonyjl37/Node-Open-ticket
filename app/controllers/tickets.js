var pg = require('pg'); 
var moment = require('moment'); 
var randtoken = require('rand-token').generator({
  chars: '0-9'
});


module.exports = function(app) {
	var controller = {};

	// Listar todos os tickets
	controller.list = function(req,res){
		pg.connect(app.connectionString, function(err, client, done) {
            var id = req.query.id;
            var key = req.query.key;
            var value = req.query.value;

            var query = " ";

            if(key == "status"){
                if(value != "all"){
                    query = " AND "+ key +" = '"+value+"' ";
                }else {
                    query = " AND status != 'excluded' ";
                }
            }

            var query = client.query("SELECT tickets.id AS id, protocol, status, subject, prompt, name  FROM tickets INNER JOIN sectors ON tickets.sector_id = sectors.id WHERE user_id = "+ id + query +" ORDER BY id DESC",function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }
            });
            var results = [];

            query.on('row', function(row) {
                results.push(row);
            });

            query.on('end', function() {
                client.end();
                return res.json(results);
            });

            if(err) {
              console.log(err);
            }

        });
	};

    controller.adminList = function(req,res){
        pg.connect(app.connectionString, function(err, client, done) {
  
            var key = req.query.key;
            var value = req.query.value;

            console.log("atributos");
            console.log(key);
             console.log(value);

            var query = " ";

            if(key == "status"){
                if(value != "all"){
                    query =  "WHERE "+key +" = '"+value+"' ";
                }
            }

            var query = client.query("SELECT tickets.id AS id, user_id, protocol, status, subject, prompt, name  FROM tickets INNER JOIN sectors ON tickets.sector_id = sectors.id " + query +" ORDER BY id DESC",function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }
            });
            var results = [];

            query.on('row', function(row) {
                results.push(row);
            });

            query.on('end', function() {
                client.end();
                return res.json(results);
            });

            if(err) {
              console.log(err);
            }

        });
    };

	// Adicionar novo ticket
	controller.new = function(req,res){

        var protocol = randtoken.generate(8);
		var seed = {
            protocol: protocol,
            status: "new",
            subject: req.body.subject,
            description: req.body.description,
            prompt: req.body.prompt,
            user_id: req.body.user_id,
            sector_id: req.body.sector_id
		};

        var attachs = req.body.attachs;

		pg.connect(app.connectionString, function(err, client, done) {

            var query = client.query(app.seeder.getInsertSQLId(seed,'tickets'),function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }
            });

            query.on('end', function(data) {
                for (var i = 0; i < attachs.length; i++) {
                    
                    client.query("UPDATE attachs SET ticket_id = "+data.rows[0].id+" WHERE path = '"+attachs[i].path+"' ;",function(err, result) {
                        if(err) {
                          console.error('error running query', err);
                          return res.status(500).send('Something broke!');
                        }
                    });
                };

                if(attachs.length > 0){
                    client.on('drain', function() {
                      console.log("drained");
                      client.end();
                      return res.send("Ticket adicionado com sucesso.");
                    });
                }else{
                    client.end();
                    return res.send("Ticket adicionado com sucesso.");
                }
            });

            if(err) {
              console.log(err);
            }

        });

	};

	// Atualizar Ticket
	controller.update = function(req,res){

		var seed = req.body.ticket;

		pg.connect(app.connectionString, function(err, client, done) {

            var query = client.query(app.seeder.getUpdateSQL(seed,'tickets'),function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }
            });

            query.on('end', function() {
                client.end();
                return res.send("Ticket atualizado com sucesso.");
            });

            if(err) {
              console.log(err);
            }

        });

	};



    controller.show = function(req,res){
        pg.connect(app.connectionString, function(err, client, done) {

            var id = req.query.id;
            var user_id = req.query.user_id;

            var query = client.query("SELECT tickets.id AS id, protocol, sectors.name AS sector, subject, status, prompt, description, users.name AS name, support_id FROM tickets INNER JOIN sectors ON tickets.sector_id = sectors.id INNER JOIN users ON tickets.user_id = users.id WHERE tickets.id = " + id + " AND tickets.user_id = "+ user_id +";",function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }

            });
            
            var results = [];

            query.on('row', function(row) {
                
                client.query("SELECT path FROM attachs WHERE ticket_id = "+ row.id +" ORDER BY id ASC;",function(err, result) {
                    if(err) {
                      console.error('error running query', err);
                      return res.status(500).send('Something broke!');
                    }
                    
                    row.attachs = result.rows;
                    results.push(row);
                    allDone();
                });
                
            });

            var allDone = function () {
                client.end();
                return res.json(results[0]);
            }

            if(err) {
              console.log(err);
            }

        });
    };


    controller.newMessage = function(req,res){

        var create_on = moment().format("YYYY-MM-DD hh:mm:ss");;
        var seed = {
            message: req.body.message,
            user_id:  req.body.user_id,
            create_on: create_on,
            ticket_id:  req.body.ticket_id,
        };

        var attachs = req.body.attachs;

        pg.connect(app.connectionString, function(err, client, done) {

            var query = client.query(app.seeder.getInsertSQLId(seed,'ticketshistory'),function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }
            });

            query.on('end', function(data) {

                for (var i = 0; i < attachs.length; i++) {
                    
                    client.query("UPDATE attachs SET message_id = "+data.rows[0].id+" WHERE path = '"+attachs[i].path+"' ;",function(err, result) {
                        if(err) {
                          console.error('error running query', err);
                          return res.status(500).send('Something broke!');
                        }
                    });
                };

                if(attachs.length > 0){
                    client.on('drain', function() {
                      console.log("drained");
                      client.end();
                      return res.send("Ticket adicionado com sucesso.");
                    });
                }else{
                    client.end();
                    return res.send("Ticket adicionado com sucesso.");
                }
                
            });

            if(err) {
              console.log(err);
            }

        });

    };

    controller.listMessages = function(req,res){
        pg.connect(app.connectionString, function(err, client, done) {
            var id = req.query.id;

            var query = client.query("SELECT ticketshistory.id AS id, users.name AS name, ticketshistory.create_on AS date, ticketshistory.message AS text FROM ticketshistory INNER JOIN users ON  ticketshistory.user_id = users.id WHERE ticket_id = "+ id +" ORDER BY id ASC",function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }
            });

            var results = [];

            query.on('row', function(row) {
                results.push(row);
            });

            query.on('end', function() {
                client.end();
                return res.json(results);
            });


            if(err) {
              console.log(err);
            }

        });
    };

    controller.messagesAttachs = function(req,res){
        pg.connect(app.connectionString, function(err, client, done) {
            var id = req.query.id;

            var query = client.query("SELECT path FROM attachs WHERE message_id = "+ id +" ORDER BY id ASC;",function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }
            });

            var results = [];

            query.on('row', function(row) {
                results.push(row);
            });

            query.on('end', function() {
                client.end();
                return res.json(results);
            });


            if(err) {
              console.log(err);
            }

        });
    };

    controller.listSupport = function(req,res){
        pg.connect(app.connectionString, function(err, client, done) {
            var id = req.query.id;
            var key = req.query.key;
            var value = req.query.value;
            var support_id = req.query.support_id;

            var query = " ";

            if(key == "status"){

                if(value != "all"){
                    query =  key +" = '"+value+"' AND";
                }

                if(value == "open"){
                    query =  key +" = '" + value + "' AND support_id = " + support_id + " AND";
                }
            }

            var query = client.query("SELECT tickets.id AS id, tickets.user_id AS user_id, protocol, status, subject, prompt, name  FROM tickets INNER JOIN sectors ON tickets.sector_id = sectors.id WHERE  "+ query +" tickets.sector_id IN (SELECT sector_id FROM sector WHERE user_id = " + id + ") ORDER BY id DESC",function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }
            });
            var results = [];

            query.on('row', function(row) {
                results.push(row);
            });

            query.on('end', function() {
                client.end();
                return res.json(results);
            });

            if(err) {
              console.log(err);
            }

        });
    };


        // Atualizar Ticket
    controller.takeToMe = function(req,res){

        var seed = {
            id: req.body.id,
            support_id: req.body.support_id,
            status: "open"
        };

        pg.connect(app.connectionString, function(err, client, done) {

            var query = client.query(app.seeder.getUpdateSQL(seed,'tickets'),function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }
            });

            query.on('end', function() {
                client.end();
                return res.send("Ticket atualizado com sucesso.");
            });

            if(err) {
              console.log(err);
            }

        });

    };

    controller.closeTicket = function(req,res){

        var seed = {
            id: req.body.id,
            status: "close"
        };

        pg.connect(app.connectionString, function(err, client, done) {

            var query = client.query(app.seeder.getUpdateSQL(seed,'tickets'),function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }
            });

            query.on('end', function() {
                client.end();
                return res.send("Ticket atualizado com sucesso.");
            });

            if(err) {
              console.log(err);
            }

        });

    };

    controller.openTicket = function(req,res){

        var seed = {
            id: req.body.id,
            status: "open"
        };

        pg.connect(app.connectionString, function(err, client, done) {

            var query = client.query(app.seeder.getUpdateSQL(seed,'tickets'),function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }
            });

            query.on('end', function() {
                client.end();
                return res.send("Ticket atualizado com sucesso.");
            });

            if(err) {
              console.log(err);
            }

        });

    };

    controller.deleteTicket = function(req,res){

        var seed = {
            id: req.body.id,
            status: "excluded"
        };

        pg.connect(app.connectionString, function(err, client, done) {

            var query = client.query(app.seeder.getUpdateSQL(seed,'tickets'),function(err, result) {
                if(err) {
                  console.error('error running query', err);
                  return res.status(500).send('Something broke!');
                }
            });

            query.on('end', function() {
                client.end();
                return res.send("Ticket atualizado com sucesso.");
            });

            if(err) {
              console.log(err);
            }

        });
    };

	return controller;
};