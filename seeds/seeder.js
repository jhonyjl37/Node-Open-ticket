
var pg = require('pg'); 
var seeder = {};

// Configurações
var connectionString = process.env.OPENSHIFT_POSTGRESQL_DB_URL || 'postgres://localhost:5432/openticket';

// Tranformar json em sql
seeder.getInsertSQL = function (seed,table) {
  var columns = [];
  var values  = [];

  for (var column in seed) {
    columns.push(column);
    values.push(seed[column]);
  }

  var sql = "INSERT INTO " + table + " (";

  for (var i = 0; i < columns.length; i++) {
    sql += columns[i];
    if(i != columns.length - 1 ){
      sql += ", ";
    } else {
      sql += ") VALUES (";
    }
  };

  for (var i = 0; i < values.length; i++) {
    sql += "'" + values[i] + "'";
    if(i != values.length - 1 ){
      sql += ", ";
    } else {
      sql += ");";
    }
  };

  return sql;
}

seeder.getInsertSQLId = function (seed,table) {
  var columns = [];
  var values  = [];

  for (var column in seed) {
    columns.push(column);
    values.push(seed[column]);
  }

  var sql = "INSERT INTO " + table + " (";

  for (var i = 0; i < columns.length; i++) {
    sql += columns[i];
    if(i != columns.length - 1 ){
      sql += ", ";
    } else {
      sql += ") VALUES (";
    }
  };

  for (var i = 0; i < values.length; i++) {
    sql += "'" + values[i] + "'";
    if(i != values.length - 1 ){
      sql += ", ";
    } else {
      sql += ") RETURNING id ;";
    }
  };

  return sql;
}


// Tranformar json em sql
seeder.getUpdateSQL = function (seed,table) {
  var columns = [];
  var values  = [];

  for (var column in seed) {
    columns.push(column);
    values.push(seed[column]);
  }

  var sql = "UPDATE " + table + " SET ";

  for (var i = 0; i < columns.length; i++) {
    if(i != columns.length-1){
      sql += columns[i] + " = '" + values[i]+"', ";
    }else{
      sql += columns[i] + " = '" + values[i]+"'";
    }
  };

  sql +=  " WHERE id = " + seed.id + " ;";
  sql = sql.replace("'null'", "NULL");

  return sql;
}

seeder.getDeleteSQL = function (seed,table) {
   return "DELETE FROM " + table + " WHERE id = " + seed.id + " ;";
};


// Inserir um conjunto de instâncias no db.
seeder.InsertGroup = function(table,seeds){

   var client = new pg.Client(connectionString);
   client.connect();
  
  for (var i = 0; i < seeds.length ; i++) {

    if( i != seeds.length - 1){
      client.query(seeder.getInsertSQL(seeds[i],table));
    }else{
      var query = client.query(seeder.getInsertSQL(seeds[i],table));
      query.on('end', function() {
        client.end();
        console.log("Foram adicionados "+seeds.length+" instâncias na tabela "+table+".");
      });
      
    }
    
  };
}


module.exports = seeder;