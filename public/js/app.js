var app = angular.module('openTicket', ['ngRoute', 'Factorys']);

var global = {};
global.domain =  "";//"http://saitesopenticket-redhateste.rhcloud.com";//"http://localhost:3000"; //

app.config(['$routeProvider',
  function($routeProvider) {
    var rp = $routeProvider;

    rp.when('/', {
      templateUrl: 'views/home.html'
    });

    rp.when('/login', {
      templateUrl: 'views/login.html',
      controller: "Login"
    });

    rp.when('/cadastrar-usuario', {
      templateUrl: 'views/users/cadastrar-usuario.html',
      controller: 'cadastrarUsuario'
    });

    rp.when('/editar-usuario', {
      templateUrl: 'views/users/editar-usuario.html',
      controller: 'editarUsuario'
    });

    rp.when('/sucesso-cadastro', {
      templateUrl: 'views/users/sucesso-cadastro.html'
    });

    rp.when('/usuarios', {
      templateUrl: 'views/users/usuarios.html',
      controller: 'Usuarios'
    });

    rp.when('/usuario', {
      templateUrl: 'views/users/usuario.html',
      controller: 'Usuario'
    });

    rp.when('/setores', {
      templateUrl: 'views/sectors/setores.html',
      controller: 'Setores'
    });

    rp.when('/dashboard', {
      templateUrl: 'views/dashboard/index.html',
      controller: 'DashboardOrdinary'
    });

    rp.when('/dashboard/suporte', {
      templateUrl: 'views/dashboard/support.html',
      controller: 'DashboardSupport'
    });

    rp.when('/dashboard/admin', {
      templateUrl: 'views/dashboard/admin.html',
      controller: 'DashboardAdmin'
    });

    rp.when('/criar-ticket', {
      templateUrl: 'views/tickets/criar-ticket.html',
      controller: 'criarTicket'
    });

    rp.when('/tickets', {
      templateUrl: 'views/tickets/tickets.html',
      controller: 'Tickets'
    });

    rp.when('/ticket', {
      templateUrl: 'views/tickets/ticket.html',
      controller: 'Ticket'
    });

    rp.when('/tickets/suporte', {
      templateUrl: 'views/tickets/ticketsSupport.html',
      controller: 'TicketsSupport'
    });

    rp.when('/usuario/mudar-senha', {
      templateUrl: 'views/users/mudar-senha.html',
      controller: 'MudarSenha'
    });

    rp.when('/usuario/mudar-senha/sucesso', {
      templateUrl: 'views/users/senha-sucesso.html'
    });

    rp.when('/ticket/transferir', {
      templateUrl: 'views/tickets/transferir.html',
      controller: 'TransferirTicket'
    });

    rp.when('/ticket/transferir/sucesso', {
      templateUrl: 'views/tickets/ticket-transferido-sucesso.html'
    });

    rp.when('/tickets/admin', {
      templateUrl: 'views/tickets/ticketsSupport.html',
      controller: 'TicketsAdmin'
    });

    rp.when('/senha/esqueci', {
      templateUrl: 'views/senha/esqueceu-sua-senha.html',
      controller: 'EsqueciSenha'
    });

    rp.when('/senha/esqueci/sucesso', {
      templateUrl: 'views/senha/sucesso.html'
    });

    rp.when('/senha/esqueci/falha', {
      templateUrl: 'views/senha/falha.html'
    });

    rp.otherwise({
      redirectTo: '/'
    });

  }]);
