app.filter('date', function() {
  return function(input, uppercase) {
    return moment(input).format("DD/MM/YYYY");
  };
});

app.filter('datetime', function() {
  return function(input, uppercase) {
    return moment(input).format("DD/MM/YYYY hh:mm:ss");
  };
});


app.filter('path', function() {
  return function(input, uppercase) {
    var trucate  = input.substring(0,9);
    var ext = input.split('.').pop();
    return trucate+"..."+ext;
  };
});



app.filter('privileges', function() {
  return function(input, uppercase) {
    switch(input){
      case "ordinary":
        return "Comum";
        break;
      case "support":
        return "Suporte";
        break;
      case "admin":
        return "Administrador";
        break;
      default:
        return "";
    }

  };
});

app.filter('gender', function() {
  return function(input, uppercase) {
    if(input == "m") {
      return "masculino";
    }else{
      return "feminino";
    }
  };
});

app.filter('sectorIcon', function() {
  return function(input, uppercase) {
    if(input > 0) {
      return "lock";
    }else{
      return "trash";
    }
  };
});

app.filter('status', function() {
  return function(input, uppercase) {
    switch(input){
      case "new":
        return "novo";
        break;
      case "open":
        return "aberto";
        break;
      case "close":
        return "fechado";
        break;
      case "excluded":
        return "excluído";
        break;
      default:
        return "";
    }
  };
});

function getGender(){
  var radios = document.getElementsByName('gender');

  for (var i = 0, length = radios.length; i < length; i++) {
      if (radios[i].checked) {
          return radios[i].value;
      }
  }
}