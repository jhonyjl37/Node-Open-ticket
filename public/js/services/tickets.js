services.factory("TicketService",function($http){
	var factory = {};

	// Lista todos os tickets com base em uma query
	factory.getTickets = function(id,query, callback){
		$http.get(global.domain + "/tickets?id="+id+"&key="+query.key+"&value="+query.value).
		  success(function(data, status, headers, config) {
		  	callback(data);
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	factory.getTicketsAdmin = function(query, callback){
		$http.get(global.domain + "/tickets/admin?key="+query.key+"&value="+query.value).
		  success(function(data, status, headers, config) {
		  	callback(data);
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	factory.createTicket = function(ticket, callback){
		$http.post(global.domain + "/tickets/new", ticket).
		  success(function(data, status, headers, config) {
		    callback();
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	factory.updateTicket = function(ticket, sucess){
		$http.post(global.domain + "/tickets/update",{ ticket: ticket}).
		  success(function(data, status, headers, config) {
		    sucess();
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	factory.getDeleteTicket = function(id, sucess, error){
		$http.post(global.domain + "/tickets/delete", { id: id }).
		  success(function(data, status, headers, config) {
		    sucess();
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  	error();
		  });
	};

	factory.getTicket = function(id,user_id,callback){
		$http.get(global.domain + "/ticket?id="+id+"&user_id="+user_id).
		  success(function(data, status, headers, config) {
		    callback(data);
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	factory.createMessage = function(message, callback){
		$http.post(global.domain + "/ticket/message/new", message).
		  success(function(data, status, headers, config) {
		    callback();
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	factory.getMessages = function(id,callback){
		$http.get(global.domain + "/ticket/messages?id="+id).
		  success(function(data, status, headers, config) {
		    callback(data);
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	// Lista todos os tickets associados a um suporte
	factory.getTicketsSupport = function(id,query, callback){
		$http.get(global.domain + "/tickets/support?id="+id+"&key="+query.key+"&value="+query.value+"&support_id="+query.support_id).
		  success(function(data, status, headers, config) {
		  	callback(data);
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	// Associa um ticket a um suporte
	factory.takeToMe = function(id,support_id, sucess, error){
		$http.post(global.domain + "/ticket/take-to-me", { id: id , support_id: support_id }).
		  success(function(data, status, headers, config) {
		    sucess();
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  	error();
		  });
	};

	factory.closeTicket = function(id, sucess, error){
		$http.post(global.domain + "/ticket/close-ticket", { id: id }).
		  success(function(data, status, headers, config) {
		    sucess();
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  	error();
		  });
	};

	factory.openTicket = function(id, sucess, error){
		$http.post(global.domain + "/ticket/open-ticket", { id: id }).
		  success(function(data, status, headers, config) {
		    sucess();
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  	error();
		  });
	};

	factory.upload = function(file, sucess, error){
        var fd = new FormData();
        fd.append('file', file);
        $http.post(global.domain + '/upload', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(data){
        	sucess(data);
        })
        .error(function(){
        	error();
        });
    };

    factory.getMessageAttachs = function(id,i,callback){
		$http.get(global.domain + "/tickets/message/attachs?id="+id).
		  success(function(data, status, headers, config) {
		  	callback(data,i);
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	return factory;
});