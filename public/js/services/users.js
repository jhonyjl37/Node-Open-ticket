services.factory("UserService",function($http){
	var factory = {};

	factory.createUser = function(user){

		$http.post(global.domain + "/users/new", user).
		  success(function(data, status, headers, config) {
		    window.location = "#/sucesso-cadastro";
		    $("html, body").animate({ scrollTop: 0 }, "fast");
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	factory.updateUser = function(user, callback){
		$http.post(global.domain + "/users/update",{ user: user}).
		  success(function(data, status, headers, config) {
		    callback();
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	factory.unique = function(data, sucess, error){

		$http.post(global.domain + "/users/unique", { key: data.key, value: data.value }).
		  success(function(data, status, headers, config) {

		  	if(data == "duplicate"){
		  		error();
		  	}else{
		  		sucess();
		  	}
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	factory.getUsers = function(callback){
		var auth = localStorage.getItem("auth");
		$http.get(global.domain + "/users?auth=" + auth).
		  success(function(data, status, headers, config) {
		  	callback(data);
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  	if(status == 403) 	window.location = "#/";
		  	callback(null);
		  });
	};

	factory.getUsersFilter = function(callback, filter){
		$http.post(global.domain + "/users/filter", filter).
		  success(function(data, status, headers, config) {
		  	callback(data);
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  	callback(null);
		  });
	};

	factory.getDeleteUser = function(callback, id){
		$http.get(global.domain + "/users/delete?id="+id).
		  success(function(data, status, headers, config) {
		  	callback(data);
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	factory.getUser = function(callback, id){
		$http.get(global.domain + "/users/show?id="+id).
		  success(function(data, status, headers, config) {
		  	callback(data);
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	factory.updatePassword = function(seed, sucess, error){
		$http.post(global.domain + "/users/update/password", seed).
		  success(function(data, status, headers, config) {
		  	sucess();
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  	error();
		  });
	};

	factory.forgetPassword = function(email, sucess, error){
		$http.post(global.domain + "/forget-password", {email: email}).
		  success(function(data, status, headers, config) {
		  	sucess();
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  	error();
		  });
	};
	

	return factory;
});