services.factory("SectorService",function($http){
	var factory = {};

	factory.getSectors = function(callback){
		$http.get(global.domain + "/sectors").
		  success(function(data, status, headers, config) {
		  	callback(data);
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	factory.createSector = function(sector, callback){
		$http.post(global.domain + "/sectors/new", sector).
		  success(function(data, status, headers, config) {
		  	callback(data);
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	factory.getDeleteSector = function(id, sucess, error){
		$http.get(global.domain + "/sectors/delete?id="+id).
		  success(function(data, status, headers, config) {
		    sucess();
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  	error();
		  });
	};

	factory.getAssociations = function(id, callback){
		$http.get(global.domain + "/sectors/associations?id="+id).
		  success(function(data, status, headers, config) {
		  	callback(data);
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	factory.getUsersBySector = function(id, callback){
		$http.get(global.domain + "/sectors/users?id="+id).
		  success(function(data, status, headers, config) {
		  	callback(data);
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	factory.createAssociation = function(assoc, callback){
		$http.post(global.domain + "/sectors/association/new", assoc).
		  success(function(data, status, headers, config) {
		  	callback(data);
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	factory.getDeleteAssociation = function(id, callback){
		$http.get(global.domain + "/sectors/association/delete?id="+id).
		  success(function(data, status, headers, config) {
		    callback();
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	return factory;
});