services.factory("PhoneService",function($http){
	var factory = {};

	factory.getPhones = function(callback, id){
		$http.get(global.domain + "/phones?id="+id).
		  success(function(data, status, headers, config) {
		  	callback(data);
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	factory.createPhone = function(phone, callback){
		$http.post(global.domain + "/phones/new", phone).
		  success(function(data, status, headers, config) {
		    callback();
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	factory.getDeletePhone = function(callback, id){
		$http.get(global.domain + "/phones/delete?id="+id).
		  success(function(data, status, headers, config) {
		    callback();
		  }).
		  error(function(data, status, headers, config) {
		  	console.log("Aconteceu alguma coisa errada.");
		  });
	};

	return factory;
});