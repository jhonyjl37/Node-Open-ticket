services.factory("AuthService",function($http){
	var factory = {};


	factory.auth = function(auth, sucess, error){
		$http.post(global.domain + "/authenticate", auth).
		  success(function(data, status, headers, config) {
		  	sucess(data);
		  }).
		  error(function(data, status, headers, config) {
		  	error();
		  });
	};

	return factory;
});