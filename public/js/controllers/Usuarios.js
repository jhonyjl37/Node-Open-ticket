app.controller("Usuarios",function($scope, UserService){

  var current_user = JSON.parse(localStorage.getItem("current_user"));
  if(current_user == null || current_user == undefined) {
    window.location = "#/login";
  }

  if (current_user.privileges != "admin") window.location = "#/dashboard";

    UserService.getUsers(function(data){
      $scope.users = data;
    });

    $scope.query = "all";

    $scope.filter = function(){
      var query = document.getElementById("query").value;
      
      if (query == "all"){
        UserService.getUsers(function(data){
          $scope.users = data;
        });
      }else{
        UserService.getUsersFilter(function(data){
          $scope.users = data;
        },{ key: "privileges", value: query });
      }

    };

    var user_id = null;
    $scope.modalDelete = function(id){
      user_id = id;
      $(".modal").show();
      $("#delete_modal").show();
    }

    $scope.hiddenModal= function(){
      $(".modal").hide();
      $("#delete_modal").hide();
    };

    $scope.delete = function(){
      $scope.hiddenModal();
      UserService.getDeleteUser(function(){
        $scope.filter();
      }, user_id);
    };
});