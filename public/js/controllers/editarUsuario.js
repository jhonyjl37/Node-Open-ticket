app.controller("editarUsuario",function($scope, UserService, $routeParams){

  var current_user = JSON.parse(localStorage.getItem("current_user"));
  if(current_user == null || current_user == undefined) {
    window.location = "#/login";
  }

  if (current_user.privileges == "admin") {
    $("#simple-nav").hide();
  }else{
    $("#nav-admin").hide();
    $("#div-privileges").hide();
    $("#div-rg").hide();
    $("#div-cpf").hide();
  }

  $scope.id = $routeParams.id;
  var origin_email = null;
  var origin_user_name = null;

  var prompt_formatted = new Formatter(document.getElementById('date_of_birth'), {
    'pattern': '{{99}}/{{99}}/{{9999}}',
    'persistent': false
  });

  var picker = new Pikaday(
    {
        field: document.getElementById('date_of_birth'),
        firstDay: 1,
        minDate: new Date('1960-01-01'),
        maxDate: new Date('2015-12-31'),
        yearRange: [1960,2015],
        format: 'DD/MM/YYYY',
        i18n: {
        previousMonth : 'Mês Anterior',
        nextMonth     : 'Próximo Mês',
        months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
        weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sab']
       }
    });



  UserService.getUser(function(data){
    origin_email = data.email;
    origin_user_name = data.user_name;

    $scope.name = data.name;
    $scope.user_name = data.user_name;
    $scope.email = data.email;
    $scope.rg = data.rg;
    $scope.cpf = data.cpf;
    $scope.date_of_birth = moment(data.date_of_birth).format("DD/MM/YYYY");
    $scope.privileges = data.privileges;
    $scope.gender = data.gender;
    $scope.campus = data.campus;
    $scope.role = data.role;
  },$routeParams.id);

  var rg_formatted = new Formatter(document.getElementById('rg'), {
    'pattern': '{{99}}.{{999}}.{{999}}-{{9}}',
    'persistent': true
  });

  var cpf_formatted = new Formatter(document.getElementById('cpf'), {
    'pattern': '{{999}}.{{999}}.{{999}}-{{99}}',
    'persistent': true
  });

  $scope.sendForm = function(){
    var name          = document.getElementById("name").value;
    var user_name     = document.getElementById("user_name").value;
    var email         = document.getElementById("email").value;
    var rg            = document.getElementById("rg").value;
    var cpf           = document.getElementById("cpf").value;
    var date_of_birth = document.getElementById("date_of_birth").value;
    var gender        = getGender();
    var privileges    = document.getElementById("privileges").value;
    var campus    = document.getElementById("campus").value;
    var role    = document.getElementById("role").value;

    var birth = moment(date_of_birth, "DD-MM-YYYY").format("YYYY-MM-DD");

    var user = {
      id: $scope.id,
      name: name,
      user_name: user_name,
      email: email,
      rg: rg,
      cpf: cpf,
      date_of_birth: birth,
      gender: gender,
      privileges: privileges,
      campus: campus,
      role: role
    };

    valited(user, function(){
      UserService.updateUser(user, function(){
        if (current_user.privileges == "admin"){
          window.location = "#/usuario?id="+user.id;
          $("html, body").animate({ scrollTop: 0 }, "fast");
        }else{
          window.location = "#/dashboard";
          $("html, body").animate({ scrollTop: 0 }, "fast");
        }
        
      });
    });
  };

  function valited(user, done){

    var inputs = [
      "name",
      "user_name",
      "email",
      "rg",
      "cpf",
      "date_of_birth",
      "gender",
      "campus",
      "role"
    ];

    for (var i = 0; i < inputs.length; i++) {
      if ($("#"+inputs[i]).val() == '') {
        $("html, body").animate({ scrollTop: 0 }, "fast");
        $("#message").text("Todos os campos devem ser preenchidos.");
        $("#message").removeClass("info");
        $("#message").removeClass("warning");
        $("#message").addClass("negative");
        return;
      }
    };

    function errorUserName(){
      $("html, body").animate({ scrollTop: 0 }, "fast");
      $("#message").text("Desculpe mas esse nome de usuário não está disponível.");
      $("#message").removeClass("info");
      $("#message").removeClass("warning");
      $("#message").addClass("negative");
    }

    function errorEmail(){
        $("html, body").animate({ scrollTop: 0 }, "fast");
        $("#message").text("Desculpe esse email não é válido. Ele já existe em nossos registros, tente novamente com outro email.");
        $("#message").removeClass("info");
        $("#message").removeClass("warning");
        $("#message").addClass("negative");
      }


    if( user.email != origin_email && origin_user_name != user.user_name){
      UserService.unique({key: "email", value: user.email},
      function(){
        UserService.unique({key: "user_name", value: user.user_name}, function(){ done(); }, errorUserName );

      },errorEmail);
    }

    if( user.email != origin_email && origin_user_name == user.user_name){
       UserService.unique({key: "email", value: user.email}, function(){ done(); }, errorEmail);
    }

    if( user.email == origin_email && origin_user_name != user.user_name){
       UserService.unique({key: "user_name", value: user.user_name}, function(){ done(); }, errorUserName);
    }

    if( user.email == origin_email && origin_user_name == user.user_name){
      done();
    }
      
  }

});