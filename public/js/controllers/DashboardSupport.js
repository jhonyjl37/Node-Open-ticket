app.controller("DashboardSupport", function($scope){
  var current_user = JSON.parse(localStorage.getItem("current_user"));
  if(current_user == null || current_user == undefined) {
  	window.location = "#/login";
  }

  if (current_user.privileges == "ordinary") window.location = "#/dashboard";
  if (current_user.privileges == "admin") window.location = "#/dashboard/admin";

  $scope.user_id = current_user.user_id;
});
