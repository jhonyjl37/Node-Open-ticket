app.controller("MudarSenha",function($scope, UserService, $routeParams){
  var current_user = JSON.parse(localStorage.getItem("current_user"));
  if(current_user == null || current_user == undefined) {
    window.location = "#/login";
  }

  $scope.password = null;
  $scope.new_password = null;
  $scope.comfirm_password = null;

  $scope.changePassword = function(){

    if( $scope.new_password !=  $scope.comfirm_password){
      $("#message").text("Cuidado, a senha nova não confere. Tente novamente.");
      $("#message").removeClass("info");
      $("#message").removeClass("negative");
      $("#message").addClass("warning");
      console.log("entre in ofiud");  
      return false;
    }

    var seed = {
      user_id: current_user.user_id,
      password: $scope.password,
      new_password: $scope.new_password 
    };

    UserService.updatePassword(seed,function(){
       window.location = "#/usuario/mudar-senha/sucesso";
    },function(){
      $("#message").text("Desculpe mas provavelmente sua senha atual está incorreta.");
      $("#message").removeClass("info");
      $("#message").removeClass("warning");
      $("#message").addClass("negative");
    });

  };

});