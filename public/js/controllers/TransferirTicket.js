app.controller("TransferirTicket",function($scope,$routeParams, TicketService, SectorService){
  
  var current_user = JSON.parse(localStorage.getItem("current_user"));
  if(current_user == null || current_user == undefined) {
    window.location = "#/login";
  }

  SectorService.getSectors(function(data){
    $scope.sectors = data;
    $scope.sector = data[0].id;
    $scope.changeUsers();
  });

  $scope.users = [];

  $scope.users.push({ name: "Nenhum", id: -1, class: "focus" });
  $scope.user_id = -1;

  $scope.changeFocus = function(id){
     $scope.user_id = id;

    for (var i = 0; i <  $scope.users.length; i++) {
       if ($scope.users[i].id != id){
        $scope.users[i].class = "";
       }else{
        $scope.users[i].class = "focus";
       }
    };
  };

  $scope.sector = null;
  $scope.changeUsers= function(){
    var sector = $scope.sector;

    SectorService.getUsersBySector(sector, function(data){
      $scope.users = [];
      $scope.users.push({ name: "Nenhum", id: -1, class: "focus" });

      if(data == null) return;

      for (var i = 0; i < data.length; i++) {
        $scope.users.push({ name: data[i].name, id: data[i].id, class: "" });
      };
      
    });
  };

  $scope.transferir = function(){
    var ticket_id = $routeParams.id;

    if($scope.user_id != -1){

      TicketService.takeToMe(ticket_id, $scope.user_id,
      function(){
        TicketService.updateTicket({id: ticket_id, sector_id: $scope.sector },function(){
           window.location = "#/ticket/transferir/sucesso";
        });
      },
      function(){

      });
    }else{
        TicketService.updateTicket({id: ticket_id, sector_id: $scope.sector , status: "new", support_id: "null"},function(){
          window.location = "#/ticket/transferir/sucesso";
        });
    }

  };

});