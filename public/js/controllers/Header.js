app.controller("Header", function($scope){
	
  $scope.sair = function(){
  	localStorage.removeItem("current_user");
  	localStorage.removeItem("auth");

  	$("#login").show();
	$("#sair").hide();
	$scope.user_name = null;
  	
  };
  
  $(document).on('authIO', function() {


	  	console.log("fui chamado");
	  	var current_user = JSON.parse(localStorage.getItem("current_user"));
  
		  if(current_user == null || current_user == undefined) {
		  	$("#login").show();
		  	$("#sair").hide();
		  }else{
		  	$("#login").hide();
		  	$("#sair").show();
		  	$scope.user_name = current_user.user_name;
		  }
  	
  });

  $(document).trigger('authIO');
});