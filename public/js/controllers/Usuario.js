app.controller("Usuario",function($scope,UserService,$routeParams,PhoneService, SectorService){
  var id = $routeParams.id;
  $scope.number = "";

  UserService.getUser(function(data){
    console.log(data);

    $scope.id = data.id;
    $scope.name = data.name;
    $scope.user_name = data.user_name;
    $scope.email = data.email;
    $scope.rg = data.rg;
    $scope.cpf = data.cpf;
    $scope.date_of_birth = data.date_of_birth;
    $scope.privileges = data.privileges;
    $scope.gender = data.gender;
    $scope.campus = data.campus;
    $scope.role = data.role;

    if($scope.privileges == "support"){
      SectorService.getAssociations($scope.id,function(data){
        $scope.my_sectors = data;
      });
      $scope.setores_src = "/views/users/setores.html";
    }

  },$routeParams.id);

  PhoneService.getPhones(function(data){
    $scope.phones = data;
  }, id);

  $scope.createPhone = function(){
    var phone = {
      user_id: id,
      number: $scope.number
    };

    PhoneService.createPhone(phone,function(){
      $scope.number = "";
      PhoneService.getPhones(function(data){
        $scope.phones = data;
      }, id);
    });
  };

  var phone_id = null;
  $scope.deletePhone = function(id){
    phone_id = id;
    $(".modal").show();
    $("#delete_modal").show();
  };

  $scope.hiddenModal = function(){
    $(".modal").hide();
    $("#delete_modal").hide();
    $("#association_delete_modal").hide();
  }

  $scope.delete = function(){
    $scope.hiddenModal();
    PhoneService.getDeletePhone(function(){
        PhoneService.getPhones(function(data){
          $scope.phones = data;
        }, id);
    },phone_id);
  };

  $scope.setores_src = "";

  SectorService.getSectors(function(data){
    $scope.sectors = data;
  })

  $scope.createAssociation = function(){
    var sector = document.getElementById("sector").value;
    var assoc = {
      user_id: $scope.id,
      sector_id: sector
    };

    SectorService.createAssociation(assoc,function(){
      SectorService.getAssociations($scope.id,function(data){
        $scope.my_sectors = data;
      });
    });
  };

  var assoc_delete = null;

  $scope.showAssociationDeleteModal =function(id ){
    assoc_delete = id;
    $(".modal").show();
    $("#association_delete_modal").show();
  }

  $scope.associationDelete = function(){
    $scope.hiddenModal();
    SectorService.getDeleteAssociation(assoc_delete,function(){
      SectorService.getAssociations($scope.id,function(data){
        $scope.my_sectors = data;
      });
    });
  };

});