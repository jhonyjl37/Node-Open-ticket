app.controller("Ticket",function($scope,$routeParams, TicketService, $route, UserService){
  var current_user = JSON.parse(localStorage.getItem("current_user"));
  if(current_user == null || current_user == undefined) {
  	window.location = "#/login";
  }

  var user_id = current_user.user_id ;
  $scope.actions = "/views/tickets/actions-ordinary.html";
  if (current_user.privileges != "ordinary"){
    user_id = $routeParams.user_id;
    $scope.actions = "/views/tickets/actions-support.html";
  }



  TicketService.getTicket($routeParams.id, user_id,function(data){
      $scope.protocol = data.protocol;
      $scope.subject  = data.subject;
      $scope.sector   = data.sector;
      $scope.status   = data.status;
      $scope.prompt   = data.prompt;
      $scope.description = data.description;
      $scope.name = data.name;
      $scope.support_id = data.support_id;
      $scope.attachs_ticket = data.attachs;

      console.log("Attachs: ");
      console.log(data.attachs);

      if(!!$scope.support_id){
        $("#takeToMe").hide();
        
        UserService.getUser(function(data){
          $scope.support_name = data.name;
        },data.support_id);
      }

      if (current_user.privileges == "admin"){
          $("#takeToMe").hide();
      }

      if ($scope.status != "open"){
        $("#closeTicket").hide();
      }

      if ($scope.status != "close"){
        $("#openAgain").hide();
      }

  });

  function updateMessages(){
    TicketService.getMessages($routeParams.id, function(data){
      $scope.messages = data;

      for (var i = 0; i < $scope.messages.length; i++){
        TicketService.getMessageAttachs($scope.messages[i].id,i, function(data,index){
          $scope.messages[index].attachs = data;
        });

      };

    });
  }

  updateMessages();

  $scope.sendMessage = function(){
    var text = document.getElementById("message").value;

    var seed = {
      message: text,
      user_id: current_user.user_id,
      ticket_id: $routeParams.id,
      attachs: $scope.attachs
    };

    TicketService.createMessage(seed,function(){
      document.getElementById("message").value = "";
      $(".text-options-sub").html("");
      $scope.attachs = [];
      updateMessages();
    });
  };

  $scope.takeToMe = function(){
    TicketService.getTicket($routeParams.id, user_id,function(data){
        if(!data.support_id){
           TicketService.takeToMe($routeParams.id,current_user.user_id,
            function(){
              $route.reload();
            },
            function(){

            });
        }else{
          $('.modal').show();
          $('#take_to_me_modal').show();
        }
    });

  };

  $scope.closeTicket = function(){
    TicketService.closeTicket($routeParams.id, function(){
      $route.reload();
    });
  };

  $scope.openAgain = function(){
    TicketService.openTicket($routeParams.id, function(){
      $route.reload();
    });
  };

  $scope.transferir = function(){
      window.location = "#/ticket/transferir?id="+$routeParams.id;
  }

  $scope.hiddenModal = function(){
    $('.modal').hide();
    $('#take_to_me_modal').hide();
    $route.reload();
  };


  $scope.attachs = [];

  $scope.addAttach = function(){
    var id = Math.random().toString(36).substr(2);
     var input = $(document.createElement('input'));
      input.attr("type", "file");
      input.trigger('click');
      input.on('change',function(){
        console.log(input.context.files[0]);
        var file = input.context.files[0];

        TicketService.upload(file, function(data){
          
          $scope.attachs.push(data);
          $("#fa"+id).remove();

          var trucate  = file.name.substring(0,9);
          var ext = file.name.split('.').pop();

          $(".text-options-sub").append("<i class=\"fa fa-file-o\"><span class=\"tip\">"+trucate+"..."+ext+"</span></i>");
        }, function(){
          
        });
        $(".text-options-sub").append("<i id=\"fa"+id+"\"class=\"fa fa-clock-o\"><span class=\"tip\">carregando...</span></i>");
      });
      
  };

  $scope.download = function(path){
    window.open(global.domain + "/uploads/"+path);
  }

});