app.controller("cadastrarUsuario",function($scope, UserService){

  var rg_formatted = new Formatter(document.getElementById('rg'), {
    'pattern': '{{99}}.{{999}}.{{999}}-{{9}}',
    'persistent': false
  });

  var cpf_formatted = new Formatter(document.getElementById('cpf'), {
    'pattern': '{{999}}.{{999}}.{{999}}-{{99}}',
    'persistent': false
  });

  var date_of_birth_formatted = new Formatter(document.getElementById('date_of_birth'), {
    'pattern': '{{99}}/{{99}}/{{9999}}',
    'persistent': false
  });

  var picker = new Pikaday(
    {
        field: document.getElementById('date_of_birth'),
        firstDay: 1,
        minDate: new Date('1960-01-01'),
        maxDate: new Date('2015-12-31'),
        yearRange: [1960,2015],
        format: 'DD/MM/YYYY',
        i18n: {
        previousMonth : 'Mês Anterior',
        nextMonth     : 'Próximo Mês',
        months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
        weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sab']
       }
    });

  $scope.sendForm = function(){
    var name          = document.getElementById("name").value;
    var user_name     = document.getElementById("user_name").value;
    var email         = document.getElementById("email").value;
    var rg            = document.getElementById("rg").value;
    var cpf           = document.getElementById("cpf").value;
    var date_of_birth = document.getElementById("date_of_birth").value;
    var gender        = getGender();
    var password      = document.getElementById("password").value;
    var privileges    = document.getElementById("privileges").value;
    var campus    = document.getElementById("campus").value;
    var role    = document.getElementById("role").value;

    var birth = moment(date_of_birth, "DD-MM-YYYY").format("YYYY-MM-DD");

    var user = {
      name: name,
      user_name: user_name,
      email: email,
      rg: rg,
      cpf: cpf,
      date_of_birth: birth,
      gender: gender,
      password: password, 
      privileges: privileges,
      campus: campus,
      role: role
    };

    valited(user, function(){
      UserService.createUser(user);
    });
  };

  function valited(user, done){

    var inputs = [
      "name",
      "user_name",
      "email",
      "rg",
      "cpf",
      "date_of_birth",
      "gender",
      "campus",
      "role"
    ];

    for (var i = 0; i < inputs.length; i++) {
      if ($("#"+inputs[i]).val() == '') {
        $("html, body").animate({ scrollTop: 0 }, "fast");
        $("#message").text("Todos os campos devem ser preenchidos.");
        $("#message").removeClass("info");
        $("#message").removeClass("warning");
        $("#message").addClass("negative");
        return;
      }
    };

    if ($("#password").val() != $("#password_comfirm").val()) {
      $("html, body").animate({ scrollTop: 0 }, "fast");
      $("#message").text("As senhas não são iguais. Tente colocar a senha novamente.");
      $("#message").removeClass("info");
      $("#message").removeClass("warning");
      $("#message").addClass("negative");
      return;
    }

    UserService.unique({key: "email", value: user.email},
    function(){
      UserService.unique({key: "user_name", value: user.user_name},
        function(){
          done();
        },
        function(){
          $("html, body").animate({ scrollTop: 0 }, "fast");
          $("#message").text("Desculpe mas esse nome de usuário não está disponível.");
          $("#message").removeClass("info");
          $("#message").removeClass("warning");
          $("#message").addClass("negative");
        }
      );
      
    },function(){
      $("html, body").animate({ scrollTop: 0 }, "fast");
      $("#message").text("Desculpe esse email não é válido. Ele já existe em nossos registros, tente novamente com outro email.");
      $("#message").removeClass("info");
      $("#message").removeClass("warning");
      $("#message").addClass("negative");
    });
      
  }

});