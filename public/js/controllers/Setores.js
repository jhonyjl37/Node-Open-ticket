app.controller("Setores",function($scope, SectorService){
  $scope.name = "";

  function updateList(){
    SectorService.getSectors(function(data){
      $scope.sectors = data;
    });
  }

  updateList();

  $scope.createSector = function(){
    var sector = {
      name: $scope.name 
    };

    SectorService.createSector(sector, function(){
      $scope.name = "";
      updateList();
    });
  };

  var sector_id = null;
  $scope.deleteSector = function(id, count){
    if(count > 0) return;
    sector_id = id;
    $(".modal").show();
    $("#delete_modal").show();
  };

  $scope.hiddenModal = function(){
    $(".modal").hide();
    $("#delete_modal").hide();
  }

  $scope.delete = function(){
    $scope.hiddenModal();
    SectorService.getDeleteSector(sector_id,function(){
      updateList();
    },function(){
      // nothing
    });
  };

});