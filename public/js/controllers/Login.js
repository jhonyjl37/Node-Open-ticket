app.controller("Login",function($scope, AuthService){
  $scope.password = "";
  $scope.user_name = "";

  $("#message").hide();

  $scope.login = function(){
    var auth = {
       password: $scope.password,
       user_name: $scope.user_name
    };

    AuthService.auth(auth,
      function(data){
        localStorage.setItem("auth",data.token);
        localStorage.setItem("current_user",JSON.stringify(data));

        $(document).trigger('authIO');
        window.location = "#/dashboard";
        
      },function(){
        $("#message").show();
        localStorage.setItem("auth",null);
      });
  };

});