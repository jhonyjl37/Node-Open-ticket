app.controller("TicketsAdmin",function($scope, TicketService){
  var current_user = JSON.parse(localStorage.getItem("current_user"));
  if(current_user == null || current_user == undefined) {
  	window.location = "#/login";
  }

  if (current_user.privileges != "admin") window.location = "#/tickets";

  TicketService.getTicketsAdmin({ key: "status" , value: "new" },function(data){
    console.log(data);
  	$scope.tickets = data;
  });

  $scope.query = "new";
  $scope.filter = function(query){
  	TicketService.getTicketsAdmin({ key: "status" , value: query },function(data){
    	$scope.tickets = data;
    });
  }

});