app.controller("TicketsSupport",function($scope, TicketService){
  var current_user = JSON.parse(localStorage.getItem("current_user"));
  if(current_user == null || current_user == undefined) {
  	window.location = "#/login";
  }

  if (current_user.privileges == "ordinary") window.location = "#/tickets";
  if (current_user.privileges == "admin") window.location = "#/tickets/admin";

  TicketService.getTicketsSupport(current_user.user_id,{ key: "status" , value: "new" , support_id: current_user.user_id },function(data){
  	$scope.tickets = data;
  });

  $scope.query = "new";
  $scope.filter = function(query){
  	TicketService.getTicketsSupport(current_user.user_id,{ key: "status" , value: query, support_id: current_user.user_id },function(data){
  	$scope.tickets = data;
  });
  }

});