app.controller("criarTicket", function($scope, SectorService, TicketService){
  var prompt_formatted = new Formatter(document.getElementById('prompt'), {
    'pattern': '{{99}}/{{99}}/{{9999}}',
    'persistent': false
  });

  SectorService.getSectors(function(data){
    $scope.sectors = data;
  })

  var current_user = JSON.parse(localStorage.getItem("current_user"));
  if(current_user == null || current_user == undefined) {
  	window.location = "#/login";
  }

  $scope.subject = null;
  $scope.description = null;
  $scope.user_id = current_user.user_id;

  var picker = new Pikaday(
  {
      field: document.getElementById('prompt'),
      firstDay: 1,
      minDate: new Date('2015-01-01'),
      maxDate: new Date('2020-12-31'),
      yearRange: [2015,2020],
      format: 'DD/MM/YYYY',
      i18n: {
      previousMonth : 'Mês Anterior',
      nextMonth     : 'Próximo Mês',
      months        : ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
      weekdays      : ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
      weekdaysShort : ['Dom','Seg','Ter','Qua','Qui','Sex','Sab']
     }
    });

  function errorForm(){
    $("#message").removeClass("info");
    $("#message").removeClass("warning");
    $("#message").addClass("negative");
  }

  function isValited(){
    if($scope.subject == '') return false ;
    if($scope.description == '') return false ;
    if(document.getElementById("prompt").value == '') return false ;
    if(document.getElementById("sector").value == '') return false ;

    return true;
  }

  $scope.cleanForm = function(){
    $scope.subject = '';
    $scope.description = '';
    document.getElementById("prompt").value = '';
    document.getElementById("sector").value = '';
  };

  $scope.createTicket = function(){

    if(!isValited()){ errorForm(); return; }

  	var prompt = moment(document.getElementById("prompt").value, "DD-MM-YYYY").format("YYYY-MM-DD");
    var sector = document.getElementById("sector").value;
  	var ticket = {
  		subject: $scope.subject,
  		description: $scope.description,
  		prompt: prompt,
  		user_id: $scope.user_id,
      sector_id: sector,
      attachs: $scope.attachs
  	};

    console.log(JSON.stringify(ticket));

  	TicketService.createTicket(ticket,function(){
  		window.location = "#/tickets";
  	});

  };

  $scope.attachs = [];

  $scope.addAttach = function(){
    var id = Math.random().toString(36).substr(2);
     var input = $(document.createElement('input'));
      input.attr("type", "file");
      input.trigger('click');
      input.on('change',function(){
        console.log(input.context.files[0]);
        var file = input.context.files[0];
        console.log(file);

        TicketService.upload(file, function(data){
          console.log(data);
          $scope.attachs.push(data);
          $("#fa"+id).remove();

          var trucate  = file.name.substring(0,9);
          var ext = file.name.split('.').pop();

          $(".text-options").append("<i class=\"fa fa-file-o\"><span class=\"tip\">"+trucate+"..."+ext+"</span></i>");
        }, function(){
          
        });
        $(".text-options").append("<i id=\"fa"+id+"\"class=\"fa fa-clock-o\"><span class=\"tip\">carregando...</span></i>");
      });
      
  };

});
