app.controller("Tickets",function($scope, TicketService){
  var current_user = JSON.parse(localStorage.getItem("current_user"));
  if(current_user == null || current_user == undefined) {
  	window.location = "#/login";
  }

  if (current_user.privileges == "support") window.location = "#/tickets/suporte";
  if (current_user.privileges == "admin") window.location = "#/tickets/admin";

  TicketService.getTickets(current_user.user_id,{ key: "status" , value: "new" },function(data){
  	$scope.tickets = data;
  });

  $scope.query = "new";
  $scope.filter = function(query){
  	TicketService.getTickets(current_user.user_id,{ key: "status" , value: query },function(data){
  	 $scope.tickets = data;
    });
  }

  var ticket_id = null;
  $scope.modalDelete = function(id){
    ticket_id = id;
    $(".modal").show();
    $("#delete_modal").show();
  }

  $scope.hiddenModal= function(){
    $(".modal").hide();
    $("#delete_modal").hide();
  };

  $scope.delete = function(){
    $scope.hiddenModal();
    TicketService.getDeleteTicket(ticket_id, function(){
      $scope.filter($scope.query);
    });
  };

});