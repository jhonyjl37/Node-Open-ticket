jest.autoMockOff();

describe('Seeder', function() {

 it('Test getInsertSQL', function() {
  var seeder = require('../seeds/seeder');

	var seed = {
		name: "Lucas",
		age: "21",
		city: "Angicos"
	};
   
   expect(seeder.getInsertSQL(seed,"users"))
   .toBe("INSERT INTO users (name, age, city) VALUES ('Lucas', '21', 'Angicos');");
 });


  it('Test getUpdateSQL', function() {
	  var seeder = require('../seeds/seeder');

		var seed = {
			id: 1,
			name: "Lucas",
			age: "21",
			city: "Angicos"
		};
	   
	   expect(seeder.getUpdateSQL(seed,"users"))
	   .toBe("UPDATE users SET id = '1', name = 'Lucas', age = '21', city = 'Angicos' WHERE id = 1 ;");
	});

  it('Test getDeleteSQL', function() {
	  var seeder = require('../seeds/seeder');

		var seed = {
			id: 1
		};
	   
	   expect(seeder.getDeleteSQL(seed,"users"))
	   .toBe("DELETE FROM users WHERE id = 1 ;");
  });


});