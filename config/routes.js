module.exports = function(app) {

	app.all('/*', function(req, res, next) {
	  res.header("Access-Control-Allow-Origin", "*");
	  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	  next();
	});

	// Auth
	// --------------------------------------------------------------------

  var authController = require('../app/controllers/auth')(app);

	app.post('/authenticate', function (req, res) {
		authController.authenticate(req, res);
	});


		// midlewares
	function authorized(req, res, next) {
	    var token = req.body.auth || req.query.auth;
	    console.log(token);

	    if (token == undefined || token == null) {
	    	res.send(403);
	    };

	    authController.isValited(token, function(result){
		    if (result) {
		        next();
		    } else {
		        res.send(403);
		    }
	    });

	}

	// Users Routes
	// --------------------------------------------------------------------
	var usersController = require('../app/controllers/users')(app);

	app.get('/users/show', function (req, res) {
  		usersController.show(req,res);
	});

	app.get('/users', authorized, function (req, res) {
  		usersController.list(req,res);
	});

	app.post('/users/new', function (req, res) {
  		usersController.new(req,res);
	});

	app.post('/users/update', function (req, res) {
  		usersController.update(req,res);
	});

	app.get('/users/delete', function (req, res) {
  		usersController.delete(req,res);
	});

	app.post('/users/unique', function (req, res) {
  		usersController.unique(req,res);
	});

	app.post('/users/filter', function (req, res) {
  		usersController.filter(req,res);
	});

	app.post('/users/update/password', function (req, res) {
  		usersController.updatePassword(req,res);
	});

    // Sector Routes
	// --------------------------------------------------------------------
	var sectorsController = require('../app/controllers/sectors')(app);

	app.get('/sectors', function (req, res) {
  		sectorsController.list(req,res);
	});

	app.post('/sectors/new', function (req, res) {
  		sectorsController.new(req,res);
	});

	app.post('/sectors/update', function (req, res) {
  		sectorsController.update(req,res);
	});

	app.get('/sectors/delete', function (req, res) {
  		sectorsController.delete(req,res);
	});

	app.get('/sectors/associations', function (req, res) {
  		sectorsController.associations(req,res);
	});

	app.post('/sectors/association/new', function (req, res) {
  		sectorsController.newAssociation(req,res);
	});

	app.get('/sectors/association/delete', function (req, res) {
  		sectorsController.deleteAssociation(req,res);
	});

	app.get('/sectors/users', function (req, res) {
  		sectorsController.getUsersBySector(req,res);
	});

	// Phones Routes
	// --------------------------------------------------------------------
	var phonesController = require('../app/controllers/phones')(app);

	app.get('/phones', function (req, res) {
  		phonesController.list(req,res);
	});

	app.post('/phones/new', function (req, res) {
  		phonesController.new(req,res);
	});

	app.post('/phones/update', function (req, res) {
  		phonesController.update(req,res);
	});

	app.get('/phones/delete', function (req, res) {
  		phonesController.delete(req,res);
	});


	// Tickets Routes
	// --------------------------------------------------------------------
	var ticketController = require('../app/controllers/tickets')(app);

	app.get('/tickets', function (req, res) {
  		ticketController.list(req,res);
	});

	app.get('/tickets/admin', function (req, res) {
  		ticketController.adminList(req,res);
	});

	app.post('/tickets/new', function (req, res) {
  		ticketController.new(req,res);
	});

	app.post('/tickets/update', function (req, res) {
  		ticketController.update(req,res);
	});

	app.post('/tickets/delete', function (req, res) {
  		ticketController.deleteTicket(req,res);
	});

	app.get('/ticket', function (req, res) {
  		ticketController.show(req,res);
	});

	app.post('/ticket/message/new', function (req, res) {
  		ticketController.newMessage(req,res);
	});

	app.get('/ticket/messages', function (req, res) {
  		ticketController.listMessages(req,res);
	});

	app.get('/tickets/support', function (req, res) {
  		ticketController.listSupport(req,res);
	});

	app.post('/ticket/take-to-me', function (req, res) {
  		ticketController.takeToMe(req,res);
	});

	app.post('/ticket/close-ticket', function (req, res) {
  		ticketController.closeTicket(req,res);
	});

	app.post('/ticket/open-ticket', function (req, res) {
  		ticketController.openTicket(req,res);
	});

	app.get('/tickets/message/attachs', function (req, res) {
  		ticketController.messagesAttachs(req,res);
	});

	// Upload files

	var attachController = require('../app/controllers/attach')(app);
	app.post('/upload', function (req, res) {

		attachController.upload(req, res);
	});

	// Forget password routes
	var forgetController = require('../app/controllers/forget')(app);

	app.post('/forget-password', function (req, res) {
  		forgetController.sendEmail(req,res);
	});

}
